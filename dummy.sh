#!/bin/bash

## Create random dir
dirname=temp$RANDOM
mkdir "$dirname"

## create random file
filename=myfile$RANDOM.txt
touch $dirname/$filename

for i in `seq 1 10000000`;
do
    echo "$i" >> $dirname/$filename
done
